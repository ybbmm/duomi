<?php
/**
 * 影片数据管理
 *
 * @version        2015年11月17日Z by 张仕锋(670513345)
 * @package        DuomiCms.Administrator
 * @copyright      Copyright (c) 2015, SamFea, Inc.
 * @link           http://www.duomicms.net
 */
require_once(dirname(__FILE__)."/config.php");
require_once(duomi_DATA."/config.user.inc.php");

if(empty($action)){
	$action = 'video';
}

if($action=="video"){
	include(duomi_ADMIN.'/html/common/admin_video_main.htm');
	exit();	
}