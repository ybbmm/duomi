<?php
/**
 * 求片管理 by zsf  QQ670513345
 *
 * 
 */
require_once(dirname(__FILE__)."/config.php");
require_once(duomi_DATA."/config.user.inc.php");
$member_seek_status = array(0=>'未处理',1=>'已处理','2'=>'不处理');
if(empty($action)){
	$action = 'list';
}
//求片列表管理
if($action == 'list'){
	
	include(duomi_ADMIN.'/html/admin_member_seek.htm');
	exit();
	
}else if($action == "no_pass"){
	$result['status'] = 0; 
	$id = isset($_POST['fid']) && is_numeric($_POST['fid']) ? $_POST['fid'] : 0;
	$query = "select * from dm_member_seek where id='{$id}'";
	$vrow = $dsql->GetOne($query);
	if(!is_array($vrow)){
		$result['error_msg']  = '没有此记录';
		echo json_encode($result);
		exit;
	}
	if($vrow['status'] != 0){
		$result['error_msg']  = '已经处理过，不能再处理';
		echo json_encode($result);
		exit;
	}
	$pass_msg = $_POST['pass_msg'];
	$dsql->ExecuteNoneQuery("update `dm_member_seek` set pass_msg='{$pass_msg}',status = 2 where id='{$id}'");
	$result['status']  = 1;
	echo json_encode($result);
	exit;
	
}else if($action =="pass"){
	$result = array();
	$result['status'] = 0; 
	//通过该求片管理
	$id = isset($_POST['fid']) && is_numeric($_POST['fid']) ? $_POST['fid'] : 0;
	//读取求片信息
	$query = "select * from dm_member_seek where id='{$id}'";
	$vrow = $dsql->GetOne($query);
	if(!is_array($vrow)){
		$result['error_msg']  = '没有此记录';
		echo json_encode($result);
		exit;
	}
	if($vrow['status'] != 0){
		$result['error_msg']  = '已经处理过，不能再处理';
		echo json_encode($result);
		exit;
	}
	
	//通过该求片管理
	$video_id = isset($_POST['video_id']) && is_numeric($_POST['video_id']) ? $_POST['video_id'] : 0;;
	//isset($id) && is_numeric($video_id) ? $video_id : 0;
	//读取求片信息
	$query = "select * from dm_data where v_id='{$video_id}'";
	$vrow2 = $dsql->GetOne($query);
	if(!is_array($vrow2)){
		$result['error_msg']  = '没有此影片';
		echo json_encode($result);
		exit;
	}
	$dsql->ExecuteNoneQuery("update `dm_member_seek` set video='{$video_id}',status = 1 where id='{$id}'");
	$result['status']  = 1;
	echo json_encode($result);
	exit;
	
}else if($action == "del"){
	$back=$Pirurl;
	//通过该求片管理
	$id = isset($id) && is_numeric($id) ? $id : 0;
	//读取求片信息
	$query = "select * from dm_member_seek where id='{$id}'";
	$vrow = $dsql->GetOne($query);
	if(!is_array($vrow)){
		ShowMsg("没有此记录",$back);
		exit();
	}
	
	
	$dsql->ExecuteNoneQuery("delete from dm_member_seek where id=".$id);
	ShowMsg("删除成功",$back);
	exit();
}else if($action == "show"){
	$back=$Pirurl;
	//通过该求片管理
	$id = isset($id) && is_numeric($id) ? $id : 0;
	//读取求片信息
	$query = "select * from dm_member_seek where id='{$id}'";
	$seek = $dsql->GetOne($query);
	if(!is_array($seek)){
		ShowMsg("没有此记录",$back);
		exit();
	}
	$member = array();
	$video = array();
	if($seek['uid']){
		$query = "select * from dm_member where id='{$seek['uid']}'";
		$member = $dsql->GetOne($query);
	}
	if($seek['video'] && $seek['status'] == 1 ){
		$query = "select * from dm_data where v_id='{$seek['video']}'";
		$video = $dsql->GetOne($query);
	}
	
	include(duomi_ADMIN.'/html/admin_member_seek_show.htm');
	exit();
	
}
