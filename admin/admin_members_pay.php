<?php
/**
 * 用户
 *
 * @version        2015年7月12日Z by 海东青
 * @package        DuomiCms.Administrator
 * @copyright      Copyright (c) 2015, SamFea, Inc.
 * @link           http://www.duomicms.net
 */
require_once(dirname(__FILE__)."/config.php");
CheckPurview();
$pay_status = array(0=>'未付款',1=>'已付款');
$dsql->SetQuery("select * from `{$cfg_dbprefix}member_group` order by g_upgrade asc");
$dsql->Execute('mytag_list');
$temp = 0;
$user_group = array();

while($row_group=$dsql->GetArray('mytag_list')){
	$user_group[$row_group['gid']] = $row_group;
}
if(empty($action))
{
	$action = '';
}
if($action=='show')
{	$back=$Pirurl;
	$id = $_GET['id'] ? (int)$_GET['id'] :  0;
	$row1=$dsql->GetOne("select * from {$cfg_dbprefix}pay_record where id={$id}");
	if(!$row1){
		ShowMsg(" 没有查询到该数据","-1");
		exit();
		
	}
	$row_group =$dsql->GetOne("select * from `{$cfg_dbprefix}member_group` where gid = {$row1['gid']}");
	$row_user =$dsql->GetOne("select * from duomi_member where username='{$row1['uid']}'");
	include(duomi_ADMIN.'/html/pay/admin_members_pay_show.htm');
	exit;

}


include(duomi_ADMIN.'/html/pay/admin_members_pay.htm');
exit();
