<?php
/**
 * 首加载页
 *
 * @version        2015年7月12日Z by 海东青
 * @package        DuomiCms.Administrator
 * @copyright      Copyright (c) 2015, SamFea, Inc.
 * @link           http://www.duomicms.net
 */
require_once(dirname(__FILE__)."/config.php");
require(duomi_INC.'/image.func.php');
	require(duomi_INC.'/inc/inc_fun_funAdmin.php');
	$verLockFile = duomi_ROOT.'/data/admin/ver.txt';
	$fp = fopen($verLockFile,'r');
	$upTime = trim(fread($fp,64));
	fclose($fp);
	//$oktime = substr($upTime,0,4).'-'.substr($upTime,4,2).'-'.substr($upTime,6,2);
	$oktimear = array(0,0,0,0,0);
	$oktimear = explode('.',$upTime);
	$oktime = $oktimear[2].'-'.$oktimear[3].'-'.$oktimear[4];
	$offUrl = SpGetNewInfo();
	include(duomi_ADMIN.'/html/index_body.htm');
	exit();
?>