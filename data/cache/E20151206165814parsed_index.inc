<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta name="applicable-device" content="pc,mobile">
<title>多米(DuomiCms)-免费开源影视管理系统-Power by DuomiCms</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="/duomiui/default/css/style.css" rel="stylesheet">
<script>var sitePath=''</script>
<script src="/duomiui/default/js/jquery-1.4.4.min.js"></script>
<script src="/duomiui/default/js/common.js"></script>
<script src="/duomiui/default/js/function.js"></script>
<script src="/duomiui/default/js/jquery.lazyload.js"></script>
<link href="/favicon.ico" type="image/x-icon" rel="icon">
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon">
<!--[if lt IE 9]>
<script src="/duomiui/default/js/html5shiv.min.js"></script>
<script src="/duomiui/default/js/respond.min.js"></script>
<![endif]-->
<script src="/duomiui/default/js/jquery.superslide.js" type="text/javascript"></script>
</head>
<body>
<div class="header-all">
  <div class="top clearfix">
    <ul class="logo"><a href="/"><img src="/duomiui/default/images/logo.jpg" title="多米(DuomiCms)"></a></ul>
    <ul class="top-nav">
      <li><a class="now" href="/">首页</a></li>
      
      <!--<li><a href="/topic/?1.html">专题</a></li>
      <li class="" _t_nav="topnav-news"><a href="/news/">新闻<i class="sjbgs"></i><i class="sjbgx"></i></a></li>-->
      <li><a href="/interface/gbook.php">留言</a></li>
    </ul>
    <ul class="search so">
      <form name="formsearch" id="formsearch" action='/search.php' method="post"  target="_self" autocomplete="off">
          <input class="input" name="searchword" type="text" id="keyword" placeholder="请输入影片或演员名" />
          <div class="so-key"><a href='/search.php?searchword=%E5%85%B3%E9%94%AE%E8%AF%8DA'>关键词A</a> <a href='/search.php?searchword=%E5%85%B3%E9%94%AE%E8%AF%8DB'>关键词B</a> <a href='/search.php?searchword=%E5%85%B3%E9%94%AE%E8%AF%8DC'>关键词C</a> <a href='/search.php?searchword=%E5%85%B3%E9%94%AE%E8%AF%8DD'>关键词D</a> <a href='/search.php?searchword=%E5%85%B3%E9%94%AE%E8%AF%8DD'>关键词D</a> </div>
          <input class="imgbt" type="submit" value=""/>
      </form>
    </ul>
	<ul class="header_member">
		<li class="bb" id="member_header">{duomicms:member}</li>
	</ul>
    <ul class="nav-qt aa">
      <li class="bb"><a href="javascript:void(0)" onMouseOver="$MH.showHistory(1);"><i class="jl"></i>观看历史</a>
      <div class="cc">
	  <script type="text/javascript" src="/duomiui/default/js/history.js"></script>
          <script type="text/javascript">$MH.limit=10;$MH.WriteHistoryBox(180,170,'font');$MH.recordHistory({name:'{playpage:name}',link:'{playpage:url}',pic:'{playpage:pic}'})</script></div>
      </li>
      <!--<li class="bb"><strong class="ma"><i class="mabg"></i>手机观看</strong>
      <div class="cc maw"><i class="ewmbg"></i><p>扫描二维码用手机观看</p></div> --> 
      </li>
      <!--li class="bb member"><strong class="ma"><i class="mbbg"></i></strong>
        <div class="cc mbp">{duomicms:member}</div> 
      </li-->
    </ul>
    <ul class="sj-search">
      <li class="sbtn2"><i class="sjbg-search"></i></li>
    </ul>
    <ul class="sj-nav">
      <li class="sbtn1"><i class="sjbg-nav"></i></li>
    </ul>
    <ul class="sj-navhome">
      <li><a href="/"><i class="sjbg-home"></i></a></li>
    </ul>  
  </div>
  <div class="nav-down clearfix">
  
  <div id="topnav-news" class="nav-down-1" style="display:none;" _t_nav="topnav-news">
    <div class="nav-down-2 clearfix">
    <ul>
      
    </ul>
    </div>
  </div>
  <div id="sj-nav-1" class="nav-down-1 sy1 sj-noover" style="display:none;" _s_nav="sj-nav-1">
    <div class="nav-down-2 sj-nav-down-2 clearfix">
    <ul> 
      <li><a href="/">首页</a></li>
    
      <li><a href="/topic/?1.html">专题</a></li>
      <li><a href="/news/">新闻</a></li>
      <li><a href="/interface/gbook.php">留言</a></li>
    </ul>
    </div>
  </div>
  <div id="sj-nav-search" class="nav-down-1 sy2 sj-noover" style="display:none;" _t_nav1="sj-nav-search">
    <div class="nav-down-2 sj-nav-down-search clearfix">
    <form name="formsearch" id="formsearch" action='/search.php' method="post"  target="_self" autocomplete="off">
          <input class="input" name="searchword" type="text" id="keyword" placeholder="输入关键词"/>
          <input class="imgbt" type="submit" value="搜索"/>
      </form>
    </div>
  </div>
</div>
</div>
<div class="topone clearfix"></div>
<div class="channel-focus">
  <div class="channel-silder layout">
    <ul class="channel-silder-cnt">
    
    </ul>
    <ul class="channel-silder-nav">
    
    </ul>
  </div>
</div>
<script type="text/javascript">
	jQuery(".channel-silder").slide({ 
		titCell:".channel-silder-nav li",
		mainCell:".channel-silder-cnt",
		delayTime:800,
		triggerTime:0,
		interTime:5000,
		pnLoop:false,
		autoPage:false,
		autoPlay:true
	});
</script>
<div class="main">
	<div class="index-area clearfix">
    <h1 class="title index-color">推荐影片</h1>
    <ul>
    
    </ul>
    </div>

</div>
<div class="ylink clearfix">
    
</div>
<div class="footer clearfix">
Copyright © 2015 多米(DuomiCms V1.2)影视管理系统 版权所有 {duomicms:runinfo}   <p class="footer-bg"><span class="bg"></span></p>
</div>
<div class="gotop"><a class="gotopbg" href="javascript:;" title="返回顶部"></a></div>
</body>
</html>
