/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.5.40 : Database - duomi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`duomi` /*!40100 DEFAULT CHARACTER SET gbk */;

USE `duomi`;

/*Table structure for table `dm_admin` */

DROP TABLE IF EXISTS `dm_admin`;

CREATE TABLE `dm_admin` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `logincount` smallint(6) NOT NULL DEFAULT '0',
  `loginip` varchar(16) NOT NULL DEFAULT '',
  `logintime` int(10) NOT NULL DEFAULT '0',
  `groupid` smallint(4) NOT NULL,
  `state` smallint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `dm_admin` */

insert  into `dm_admin`(`id`,`name`,`password`,`logincount`,`loginip`,`logintime`,`groupid`,`state`) values (1,'admin','f297a57a5a743894a0e4',0,'127.0.0.1',1446360031,1,1);

/*Table structure for table `dm_arcrank` */

DROP TABLE IF EXISTS `dm_arcrank`;

CREATE TABLE `dm_arcrank` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `rank` smallint(6) NOT NULL DEFAULT '0',
  `membername` char(20) NOT NULL DEFAULT '',
  `adminrank` smallint(6) NOT NULL DEFAULT '0',
  `money` smallint(8) unsigned NOT NULL DEFAULT '500',
  `scores` mediumint(8) NOT NULL DEFAULT '0',
  `purviews` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_arcrank` */

/*Table structure for table `dm_co_cls` */

DROP TABLE IF EXISTS `dm_co_cls`;

CREATE TABLE `dm_co_cls` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `clsname` varchar(50) NOT NULL DEFAULT '',
  `sysclsid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sysclsid` (`sysclsid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_cls` */

insert  into `dm_co_cls`(`id`,`clsname`,`sysclsid`,`cotype`) values (1,'大陆',0,0),(2,'香港',0,0),(3,'台湾',0,0),(4,'日本',0,0),(5,'韩国',0,0),(6,'欧美',0,0),(7,'日韩',0,0),(8,'美国',0,0);

/*Table structure for table `dm_co_config` */

DROP TABLE IF EXISTS `dm_co_config`;

CREATE TABLE `dm_co_config` (
  `cid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) NOT NULL DEFAULT '',
  `getlistnum` int(10) NOT NULL DEFAULT '0',
  `getconnum` int(10) NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_config` */

/*Table structure for table `dm_co_data` */

DROP TABLE IF EXISTS `dm_co_data`;

CREATE TABLE `dm_co_data` (
  `v_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `tname` char(60) NOT NULL DEFAULT '',
  `v_name` char(60) NOT NULL DEFAULT '',
  `v_state` int(10) unsigned NOT NULL DEFAULT '0',
  `v_pic` char(100) NOT NULL DEFAULT '',
  `v_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_money` smallint(6) NOT NULL DEFAULT '0',
  `v_rank` smallint(6) NOT NULL DEFAULT '0',
  `v_digg` smallint(6) NOT NULL DEFAULT '0',
  `v_tread` smallint(6) NOT NULL DEFAULT '0',
  `v_commend` smallint(6) NOT NULL DEFAULT '0',
  `v_wrong` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_director` varchar(200) NOT NULL DEFAULT '',
  `v_enname` varchar(200) NOT NULL DEFAULT '',
  `v_lang` varchar(200) NOT NULL DEFAULT '',
  `v_actor` varchar(200) NOT NULL DEFAULT '',
  `v_color` char(7) NOT NULL DEFAULT '',
  `v_publishyear` char(20) NOT NULL DEFAULT '0',
  `v_publisharea` char(20) NOT NULL DEFAULT '',
  `v_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `v_topic` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_note` char(30) NOT NULL DEFAULT '',
  `v_tags` char(30) NOT NULL DEFAULT '',
  `v_letter` char(3) NOT NULL DEFAULT '',
  `v_from` char(255) NOT NULL DEFAULT '',
  `v_inbase` enum('0','1') NOT NULL DEFAULT '0',
  `v_des` text,
  `v_playdata` text,
  `v_downdata` text,
  PRIMARY KEY (`v_id`),
  KEY `tid` (`v_rank`,`tid`,`v_commend`,`v_hit`),
  KEY `v_addtime` (`v_addtime`,`v_digg`,`v_tread`,`v_inbase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_data` */

/*Table structure for table `dm_co_filters` */

DROP TABLE IF EXISTS `dm_co_filters`;

CREATE TABLE `dm_co_filters` (
  `ID` mediumint(8) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `rColumn` tinyint(1) NOT NULL,
  `uesMode` tinyint(1) NOT NULL,
  `sFind` varchar(255) NOT NULL,
  `sStart` varchar(255) NOT NULL,
  `sEnd` varchar(255) NOT NULL,
  `sReplace` varchar(255) NOT NULL,
  `Flag` tinyint(1) NOT NULL,
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_filters` */

/*Table structure for table `dm_co_news` */

DROP TABLE IF EXISTS `dm_co_news`;

CREATE TABLE `dm_co_news` (
  `n_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `n_title` char(60) NOT NULL DEFAULT '',
  `n_keyword` varchar(80) DEFAULT NULL,
  `n_pic` char(255) NOT NULL DEFAULT '',
  `n_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `n_author` varchar(80) DEFAULT NULL,
  `n_addtime` int(10) NOT NULL DEFAULT '0',
  `n_letter` char(3) NOT NULL DEFAULT '',
  `n_content` mediumtext,
  `n_outline` char(255) DEFAULT NULL,
  `tname` char(60) NOT NULL DEFAULT '',
  `n_from` char(50) NOT NULL DEFAULT '',
  `n_inbase` enum('0','1') NOT NULL DEFAULT '0',
  `n_entitle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`n_id`),
  KEY `tid` (`tid`,`n_hit`),
  KEY `v_addtime` (`n_inbase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_news` */

/*Table structure for table `dm_co_type` */

DROP TABLE IF EXISTS `dm_co_type`;

CREATE TABLE `dm_co_type` (
  `tid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tname` varchar(50) NOT NULL DEFAULT '',
  `siteurl` char(200) NOT NULL DEFAULT '',
  `getherday` smallint(5) unsigned NOT NULL DEFAULT '0',
  `playfrom` varchar(50) NOT NULL DEFAULT '',
  `autocls` enum('0','1') NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `coding` varchar(10) NOT NULL DEFAULT 'gb2312',
  `sock` enum('0','1') NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `cjtime` int(10) unsigned NOT NULL DEFAULT '0',
  `listconfig` text,
  `itemconfig` text,
  `isok` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`),
  KEY `cid` (`cid`,`classid`),
  KEY `addtime` (`addtime`,`cjtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_type` */

/*Table structure for table `dm_co_url` */

DROP TABLE IF EXISTS `dm_co_url`;

CREATE TABLE `dm_co_url` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `url` char(255) NOT NULL DEFAULT '',
  `pic` char(255) NOT NULL DEFAULT '',
  `succ` enum('0','1') NOT NULL DEFAULT '0',
  `err` int(5) NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `cid` (`cid`,`tid`),
  KEY `succ` (`succ`,`err`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_co_url` */

/*Table structure for table `dm_comment` */

DROP TABLE IF EXISTS `dm_comment`;

CREATE TABLE `dm_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `typeid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `ip` char(15) NOT NULL DEFAULT '',
  `ischeck` smallint(6) NOT NULL DEFAULT '0',
  `dtime` int(10) unsigned NOT NULL DEFAULT '0',
  `msg` text,
  `m_type` int(6) unsigned NOT NULL DEFAULT '0',
  `reply` int(6) unsigned NOT NULL DEFAULT '0',
  `agree` int(6) unsigned NOT NULL DEFAULT '0',
  `anti` int(6) unsigned NOT NULL DEFAULT '0',
  `pic` char(255) NOT NULL DEFAULT '',
  `vote` int(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `v_id` (`v_id`,`ischeck`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_comment` */

/*Table structure for table `dm_content` */

DROP TABLE IF EXISTS `dm_content`;

CREATE TABLE `dm_content` (
  `v_id` mediumint(8) NOT NULL DEFAULT '0',
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `body` mediumtext,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_content` */

insert  into `dm_content`(`v_id`,`tid`,`body`) values (1,5,'fffffffffffff');

/*Table structure for table `dm_count` */

DROP TABLE IF EXISTS `dm_count`;

CREATE TABLE `dm_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userip` varchar(16) DEFAULT NULL,
  `serverurl` varchar(255) DEFAULT NULL,
  `updatetime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `dm_count` */

/*Table structure for table `dm_crons` */

DROP TABLE IF EXISTS `dm_crons`;

CREATE TABLE `dm_crons` (
  `cronid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `available` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('user','system') NOT NULL DEFAULT 'user',
  `name` char(50) NOT NULL DEFAULT '',
  `filename` char(255) NOT NULL DEFAULT '',
  `lastrun` int(10) unsigned NOT NULL DEFAULT '0',
  `nextrun` int(10) unsigned NOT NULL DEFAULT '0',
  `weekday` tinyint(1) NOT NULL DEFAULT '0',
  `day` tinyint(2) NOT NULL DEFAULT '0',
  `hour` tinyint(2) NOT NULL DEFAULT '0',
  `minute` char(36) NOT NULL DEFAULT '',
  PRIMARY KEY (`cronid`),
  KEY `nextrun` (`available`,`nextrun`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_crons` */

/*Table structure for table `dm_data` */

DROP TABLE IF EXISTS `dm_data`;

CREATE TABLE `dm_data` (
  `v_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_name` char(60) NOT NULL DEFAULT '',
  `v_state` int(10) unsigned NOT NULL DEFAULT '0',
  `v_pic` char(255) NOT NULL DEFAULT '',
  `v_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_money` smallint(6) NOT NULL DEFAULT '0',
  `v_rank` smallint(6) NOT NULL DEFAULT '0',
  `v_digg` smallint(6) NOT NULL DEFAULT '0',
  `v_tread` smallint(6) NOT NULL DEFAULT '0',
  `v_commend` smallint(6) NOT NULL DEFAULT '0',
  `v_wrong` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_ismake` smallint(1) unsigned NOT NULL DEFAULT '0',
  `v_actor` varchar(200) DEFAULT NULL,
  `v_color` char(7) NOT NULL DEFAULT '',
  `v_publishyear` char(20) NOT NULL DEFAULT '0',
  `v_publisharea` char(20) NOT NULL DEFAULT '',
  `v_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `v_topic` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_note` char(30) NOT NULL DEFAULT '',
  `v_tags` char(30) NOT NULL DEFAULT '',
  `v_letter` char(3) NOT NULL DEFAULT '',
  `v_isunion` smallint(6) NOT NULL DEFAULT '0',
  `v_recycled` smallint(6) NOT NULL DEFAULT '0',
  `v_director` varchar(200) DEFAULT NULL,
  `v_enname` varchar(200) DEFAULT NULL,
  `v_lang` varchar(200) DEFAULT NULL,
  `v_score` bigint(10) DEFAULT '0',
  `v_scorenum` int(10) DEFAULT '0',
  `v_extratype` text,
  `v_jq` text,
  `v_nickname` char(60) DEFAULT NULL,
  `v_reweek` char(60) DEFAULT NULL,
  `v_douban` varchar(3) DEFAULT NULL,
  `v_mtime` varchar(3) DEFAULT NULL,
  `v_imdb` varchar(3) DEFAULT NULL,
  `v_tvs` char(60) DEFAULT NULL,
  `v_company` char(60) DEFAULT NULL,
  `v_dayhit` int(10) DEFAULT NULL,
  `v_weekhit` int(10) DEFAULT NULL,
  `v_monthhit` int(10) DEFAULT NULL,
  `v_daytime` int(10) DEFAULT NULL,
  `v_weektime` int(10) DEFAULT NULL,
  `v_monthtime` int(10) DEFAULT NULL,
  `v_len` varchar(6) DEFAULT NULL,
  `v_total` varchar(6) DEFAULT NULL,
  `v_ver` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`v_id`),
  KEY `idx_tid` (`tid`,`v_recycled`,`v_addtime`),
  KEY `idx_addtime` (`v_addtime`),
  KEY `idx_name` (`v_name`,`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `dm_data` */

insert  into `dm_data`(`v_id`,`tid`,`v_name`,`v_state`,`v_pic`,`v_hit`,`v_money`,`v_rank`,`v_digg`,`v_tread`,`v_commend`,`v_wrong`,`v_ismake`,`v_actor`,`v_color`,`v_publishyear`,`v_publisharea`,`v_addtime`,`v_topic`,`v_note`,`v_tags`,`v_letter`,`v_isunion`,`v_recycled`,`v_director`,`v_enname`,`v_lang`,`v_score`,`v_scorenum`,`v_extratype`,`v_jq`,`v_nickname`,`v_reweek`,`v_douban`,`v_mtime`,`v_imdb`,`v_tvs`,`v_company`,`v_dayhit`,`v_weekhit`,`v_monthhit`,`v_daytime`,`v_weektime`,`v_monthtime`,`v_len`,`v_total`,`v_ver`) values (1,5,'测试',0,'',1,0,0,0,0,2,0,0,'a','','2015','',1446360208,0,'ff','fff','C',0,0,'a','ceshi','',0,0,'1,5','','','','0','0','0','','',1,1,1,1446360168,1446360168,1446360168,'','','');

/*Table structure for table `dm_erradd` */

DROP TABLE IF EXISTS `dm_erradd`;

CREATE TABLE `dm_erradd` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vid` mediumint(8) unsigned NOT NULL,
  `author` char(60) NOT NULL DEFAULT '',
  `ip` char(15) NOT NULL DEFAULT '',
  `errtxt` mediumtext,
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_erradd` */

/*Table structure for table `dm_favorite` */

DROP TABLE IF EXISTS `dm_favorite`;

CREATE TABLE `dm_favorite` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `vid` int(11) NOT NULL DEFAULT '0',
  `kptime` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_favorite` */

/*Table structure for table `dm_flink` */

DROP TABLE IF EXISTS `dm_flink`;

CREATE TABLE `dm_flink` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `sortrank` smallint(6) NOT NULL DEFAULT '0',
  `url` char(60) NOT NULL DEFAULT '',
  `webname` char(30) NOT NULL DEFAULT '',
  `msg` char(200) NOT NULL DEFAULT '',
  `email` char(50) NOT NULL DEFAULT '',
  `logo` char(60) NOT NULL DEFAULT '',
  `dtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ischeck` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `dm_flink` */

insert  into `dm_flink`(`id`,`sortrank`,`url`,`webname`,`msg`,`email`,`logo`,`dtime`,`ischeck`) values (1,0,'http://www.duomicms.net','多米影视管理系统','','','',1432312055,1);

/*Table structure for table `dm_guestbook` */

DROP TABLE IF EXISTS `dm_guestbook`;

CREATE TABLE `dm_guestbook` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title` varchar(60) NOT NULL DEFAULT '',
  `mid` mediumint(8) unsigned DEFAULT '0',
  `posttime` int(10) unsigned NOT NULL DEFAULT '0',
  `uname` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `dtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ischeck` smallint(6) NOT NULL DEFAULT '1',
  `msg` text,
  PRIMARY KEY (`id`),
  KEY `ischeck` (`ischeck`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_guestbook` */

/*Table structure for table `dm_jqtype` */

DROP TABLE IF EXISTS `dm_jqtype`;

CREATE TABLE `dm_jqtype` (
  `tid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `tname` char(30) NOT NULL DEFAULT '',
  `ishidden` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_jqtype` */

/*Table structure for table `dm_member` */

DROP TABLE IF EXISTS `dm_member`;

CREATE TABLE `dm_member` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `nickname` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` char(255) NOT NULL DEFAULT '',
  `logincount` smallint(6) NOT NULL DEFAULT '0',
  `regip` varchar(16) NOT NULL DEFAULT '',
  `regtime` int(10) NOT NULL DEFAULT '0',
  `gid` smallint(4) NOT NULL,
  `points` int(10) NOT NULL DEFAULT '0',
  `state` smallint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_member` */

/*Table structure for table `dm_member_group` */

DROP TABLE IF EXISTS `dm_member_group`;

CREATE TABLE `dm_member_group` (
  `gid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gname` varchar(32) NOT NULL DEFAULT '',
  `gtype` varchar(255) NOT NULL DEFAULT '',
  `g_auth` varchar(32) NOT NULL DEFAULT '',
  `g_upgrade` int(11) NOT NULL DEFAULT '0',
  `g_authvalue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `dm_member_group` */

insert  into `dm_member_group`(`gid`,`gname`,`gtype`,`g_auth`,`g_upgrade`,`g_authvalue`) values (1,'游客用户','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15','1,2,3',0,0),(2,'普通会员','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15','1,2,3',10,0);

/*Table structure for table `dm_myad` */

DROP TABLE IF EXISTS `dm_myad`;

CREATE TABLE `dm_myad` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `adname` varchar(100) NOT NULL DEFAULT '',
  `adenname` varchar(60) NOT NULL DEFAULT '',
  `timeset` int(10) unsigned NOT NULL DEFAULT '0',
  `intro` char(255) NOT NULL DEFAULT '',
  `adsbody` text,
  PRIMARY KEY (`aid`),
  KEY `timeset` (`timeset`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `dm_myad` */

insert  into `dm_myad`(`aid`,`adname`,`adenname`,`timeset`,`intro`,`adsbody`) values (1,'article-right-ad','article-right-ad',1444999917,'内容页右侧广告位300*300','document.writeln(\"<img src=\"/duomiui/default/images/ad-300.jpg\">\")');

/*Table structure for table `dm_mytag` */

DROP TABLE IF EXISTS `dm_mytag`;

CREATE TABLE `dm_mytag` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tagname` varchar(30) NOT NULL DEFAULT '',
  `tagdes` varchar(50) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `tagcontent` text,
  PRIMARY KEY (`aid`),
  KEY `tagname` (`tagname`,`addtime`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `dm_mytag` */

insert  into `dm_mytag`(`aid`,`tagname`,`tagdes`,`addtime`,`tagcontent`) values (1,'areasearch','地区搜索',1251590919,'<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=大陆\' target=\"_blank\">大陆</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=香港\'target=\"_blank\">香港</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=台湾\'target=\"_blank\">台湾</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=日本\' target=\"_blank\">日本</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=韩国\' target=\"_blank\">韩国</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=欧美\' target=\"_blank\">欧美</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=其它\' target=\"_blank\">其它</a>'),(2,'yearsearch','按发行年份查看电影',1251509338,'<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2009\' target=\"_blank\">2009</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2008\'target=\"_blank\">2008</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2007\' target=\"_blank\">2007</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2006\' target=\"_blank\">2006</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2005\' target=\"_blank\">2005</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2004\' target=\"_blank\">2004</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2003\' target=\"_blank\">2003</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2002\' target=\"_blank\">2002</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2001\' target=\"_blank\">2001</a>'),(3,'actorsearch','演员名字',1251590973,'<a href=\'/{duomicms:sitepath}search.php?searchtype=1&searchword=成龙\' target=\"_blank\">成龙</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=1&searchword=周星驰\'target=\"_blank\">周星驰</a> ');

/*Table structure for table `dm_news` */

DROP TABLE IF EXISTS `dm_news`;

CREATE TABLE `dm_news` (
  `n_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `n_title` char(60) NOT NULL DEFAULT '',
  `n_pic` char(100) NOT NULL DEFAULT '',
  `n_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `n_money` smallint(6) NOT NULL DEFAULT '0',
  `n_rank` smallint(6) NOT NULL DEFAULT '0',
  `n_digg` smallint(6) NOT NULL DEFAULT '0',
  `n_tread` smallint(6) NOT NULL DEFAULT '0',
  `n_commend` smallint(6) NOT NULL DEFAULT '0',
  `n_author` varchar(80) DEFAULT NULL,
  `n_color` char(7) NOT NULL DEFAULT '',
  `n_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `n_note` smallint(6) NOT NULL DEFAULT '0',
  `n_letter` char(3) NOT NULL DEFAULT '',
  `n_isunion` smallint(6) NOT NULL DEFAULT '0',
  `n_recycled` smallint(6) NOT NULL DEFAULT '0',
  `n_entitle` varchar(200) DEFAULT NULL,
  `n_outline` varchar(255) DEFAULT NULL,
  `n_keyword` varchar(80) DEFAULT NULL,
  `n_from` varchar(50) DEFAULT NULL,
  `n_score` bigint(10) DEFAULT '0',
  `n_scorenum` int(10) DEFAULT '0',
  `n_content` mediumtext,
  PRIMARY KEY (`n_id`),
  KEY `tid` (`n_rank`,`tid`,`n_commend`,`n_hit`),
  KEY `v_addtime` (`n_addtime`,`n_digg`,`n_tread`,`n_isunion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_news` */

/*Table structure for table `dm_playdata` */

DROP TABLE IF EXISTS `dm_playdata`;

CREATE TABLE `dm_playdata` (
  `v_id` mediumint(8) NOT NULL DEFAULT '0',
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `body` mediumtext,
  `body1` mediumtext,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_playdata` */

insert  into `dm_playdata`(`v_id`,`tid`,`body`,`body1`) values (1,5,'ckplayer$$第1集$http://localhost/duomi/video/hwcheck.mp4$ckplayer','');

/*Table structure for table `dm_search_keywords` */

DROP TABLE IF EXISTS `dm_search_keywords`;

CREATE TABLE `dm_search_keywords` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` char(30) NOT NULL DEFAULT '',
  `spwords` char(50) NOT NULL DEFAULT '',
  `count` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `result` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_search_keywords` */

/*Table structure for table `dm_tags` */

DROP TABLE IF EXISTS `dm_tags`;

CREATE TABLE `dm_tags` (
  `tagid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag` char(30) NOT NULL DEFAULT '',
  `usenum` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `vids` text NOT NULL,
  PRIMARY KEY (`tagid`),
  KEY `usenum` (`usenum`),
  KEY `tag` (`tag`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `dm_tags` */

insert  into `dm_tags`(`tagid`,`tag`,`usenum`,`vids`) values (1,'fff',2,'1,1');

/*Table structure for table `dm_temp` */

DROP TABLE IF EXISTS `dm_temp`;

CREATE TABLE `dm_temp` (
  `v_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_name` char(60) NOT NULL DEFAULT '',
  `v_state` int(10) unsigned NOT NULL DEFAULT '0',
  `v_pic` char(100) NOT NULL DEFAULT '',
  `v_actor` varchar(200) DEFAULT NULL,
  `v_publishyear` char(20) NOT NULL DEFAULT '0',
  `v_publisharea` char(20) NOT NULL DEFAULT '',
  `v_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `v_note` char(30) NOT NULL DEFAULT '',
  `v_letter` char(3) NOT NULL DEFAULT '',
  `v_playdata` mediumtext,
  `v_des` mediumtext,
  `v_director` varchar(200) DEFAULT NULL,
  `v_enname` varchar(200) DEFAULT NULL,
  `v_lang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`v_id`),
  KEY `tid` (`tid`),
  KEY `v_addtime` (`v_addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_temp` */

/*Table structure for table `dm_topic` */

DROP TABLE IF EXISTS `dm_topic`;

CREATE TABLE `dm_topic` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL DEFAULT '',
  `enname` char(60) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0',
  `template` char(50) NOT NULL DEFAULT '',
  `pic` char(80) NOT NULL DEFAULT '',
  `des` text,
  `vod` text NOT NULL,
  `keyword` text,
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `dm_topic` */

/*Table structure for table `dm_type` */

DROP TABLE IF EXISTS `dm_type`;

CREATE TABLE `dm_type` (
  `tid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `upid` tinyint(6) unsigned NOT NULL DEFAULT '0',
  `tname` char(30) NOT NULL DEFAULT '',
  `tenname` char(60) NOT NULL DEFAULT '',
  `torder` int(11) NOT NULL DEFAULT '0',
  `templist` char(50) NOT NULL DEFAULT '',
  `templist_1` char(50) NOT NULL DEFAULT '',
  `title` char(50) NOT NULL DEFAULT '',
  `keyword` char(50) NOT NULL DEFAULT '',
  `description` char(50) NOT NULL DEFAULT '',
  `ishidden` smallint(6) NOT NULL DEFAULT '0',
  `unionid` mediumtext,
  `tptype` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`),
  KEY `upid` (`upid`,`ishidden`),
  KEY `torder` (`torder`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `dm_type` */

insert  into `dm_type`(`tid`,`upid`,`tname`,`tenname`,`torder`,`templist`,`templist_1`,`title`,`keyword`,`description`,`ishidden`,`unionid`,`tptype`) values (1,0,'电影','video',1,'channel.html','content.html','','','',0,'31_1',0),(2,0,'电视剧','tv',2,'channel.html','content.html','','','',0,'',0),(3,0,'综艺','variety',4,'channel.html','content.html','','','',0,'',0),(4,0,'动漫','animation',3,'channel.html','content.html','','','',0,'',0),(5,1,'动作片','action',5,'channel.html','content.html','','','',0,'',0),(6,1,'喜剧片','comedy',6,'channel.html','content.html','','','',0,'',0),(7,1,'爱情片','love',7,'channel.html','content.html','','','',0,'',0),(8,1,'科幻片','fiction',8,'channel.html','content.html','','','',0,'',0),(9,1,'剧情片','plot',9,'channel.html','content.html','','','',0,'',0),(10,1,'恐怖片','terror',10,'channel.html','content.html','','','',0,'',0),(11,1,'战争片','warfare',11,'channel.html','content.html','','','',0,'',0),(12,2,'大陆剧','mainland',13,'channel.html','content.html','','','',0,'',0),(13,2,'港台剧','gangtai',14,'channel.html','content.html','','','',0,'',0),(14,2,'日韩剧','rihan',15,'channel.html','content.html','','','',0,'',0),(15,2,'欧美剧','oumei',16,'channel.html','content.html','','','',0,'',0),(16,0,'影视资讯','videonews',17,'newspage.html','news.html','','','',0,'',1),(17,0,'剧情介绍','videoplot',18,'newspage.html','news.html','','','',0,'',1),(18,0,'午夜','midnight',5,'channel.html','content.html','','','',0,NULL,0);

/*Table structure for table `nr_admin` */

DROP TABLE IF EXISTS `nr_admin`;

CREATE TABLE `nr_admin` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `logincount` smallint(6) NOT NULL DEFAULT '0',
  `loginip` varchar(16) NOT NULL DEFAULT '',
  `logintime` int(10) NOT NULL DEFAULT '0',
  `groupid` smallint(4) NOT NULL,
  `state` smallint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nr_admin` */

insert  into `nr_admin`(`id`,`name`,`password`,`logincount`,`loginip`,`logintime`,`groupid`,`state`) values (1,'admin','f297a57a5a743894a0e4',0,'unknown',1446349390,1,1);

/*Table structure for table `nr_arcrank` */

DROP TABLE IF EXISTS `nr_arcrank`;

CREATE TABLE `nr_arcrank` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `rank` smallint(6) NOT NULL DEFAULT '0',
  `membername` char(20) NOT NULL DEFAULT '',
  `adminrank` smallint(6) NOT NULL DEFAULT '0',
  `money` smallint(8) unsigned NOT NULL DEFAULT '500',
  `scores` mediumint(8) NOT NULL DEFAULT '0',
  `purviews` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_arcrank` */

/*Table structure for table `nr_co_cls` */

DROP TABLE IF EXISTS `nr_co_cls`;

CREATE TABLE `nr_co_cls` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `clsname` varchar(50) NOT NULL DEFAULT '',
  `sysclsid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sysclsid` (`sysclsid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_cls` */

insert  into `nr_co_cls`(`id`,`clsname`,`sysclsid`,`cotype`) values (1,'大陆',0,0),(2,'香港',0,0),(3,'台湾',0,0),(4,'日本',0,0),(5,'韩国',0,0),(6,'欧美',0,0),(7,'日韩',0,0),(8,'美国',0,0);

/*Table structure for table `nr_co_config` */

DROP TABLE IF EXISTS `nr_co_config`;

CREATE TABLE `nr_co_config` (
  `cid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) NOT NULL DEFAULT '',
  `getlistnum` int(10) NOT NULL DEFAULT '0',
  `getconnum` int(10) NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_config` */

/*Table structure for table `nr_co_data` */

DROP TABLE IF EXISTS `nr_co_data`;

CREATE TABLE `nr_co_data` (
  `v_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `tname` char(60) NOT NULL DEFAULT '',
  `v_name` char(60) NOT NULL DEFAULT '',
  `v_state` int(10) unsigned NOT NULL DEFAULT '0',
  `v_pic` char(100) NOT NULL DEFAULT '',
  `v_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_money` smallint(6) NOT NULL DEFAULT '0',
  `v_rank` smallint(6) NOT NULL DEFAULT '0',
  `v_digg` smallint(6) NOT NULL DEFAULT '0',
  `v_tread` smallint(6) NOT NULL DEFAULT '0',
  `v_commend` smallint(6) NOT NULL DEFAULT '0',
  `v_wrong` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_director` varchar(200) NOT NULL DEFAULT '',
  `v_enname` varchar(200) NOT NULL DEFAULT '',
  `v_lang` varchar(200) NOT NULL DEFAULT '',
  `v_actor` varchar(200) NOT NULL DEFAULT '',
  `v_color` char(7) NOT NULL DEFAULT '',
  `v_publishyear` char(20) NOT NULL DEFAULT '0',
  `v_publisharea` char(20) NOT NULL DEFAULT '',
  `v_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `v_topic` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_note` char(30) NOT NULL DEFAULT '',
  `v_tags` char(30) NOT NULL DEFAULT '',
  `v_letter` char(3) NOT NULL DEFAULT '',
  `v_from` char(255) NOT NULL DEFAULT '',
  `v_inbase` enum('0','1') NOT NULL DEFAULT '0',
  `v_des` text,
  `v_playdata` text,
  `v_downdata` text,
  PRIMARY KEY (`v_id`),
  KEY `tid` (`v_rank`,`tid`,`v_commend`,`v_hit`),
  KEY `v_addtime` (`v_addtime`,`v_digg`,`v_tread`,`v_inbase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_data` */

/*Table structure for table `nr_co_filters` */

DROP TABLE IF EXISTS `nr_co_filters`;

CREATE TABLE `nr_co_filters` (
  `ID` mediumint(8) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `rColumn` tinyint(1) NOT NULL,
  `uesMode` tinyint(1) NOT NULL,
  `sFind` varchar(255) NOT NULL,
  `sStart` varchar(255) NOT NULL,
  `sEnd` varchar(255) NOT NULL,
  `sReplace` varchar(255) NOT NULL,
  `Flag` tinyint(1) NOT NULL,
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_filters` */

/*Table structure for table `nr_co_news` */

DROP TABLE IF EXISTS `nr_co_news`;

CREATE TABLE `nr_co_news` (
  `n_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `n_title` char(60) NOT NULL DEFAULT '',
  `n_keyword` varchar(80) DEFAULT NULL,
  `n_pic` char(255) NOT NULL DEFAULT '',
  `n_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `n_author` varchar(80) DEFAULT NULL,
  `n_addtime` int(10) NOT NULL DEFAULT '0',
  `n_letter` char(3) NOT NULL DEFAULT '',
  `n_content` mediumtext,
  `n_outline` char(255) DEFAULT NULL,
  `tname` char(60) NOT NULL DEFAULT '',
  `n_from` char(50) NOT NULL DEFAULT '',
  `n_inbase` enum('0','1') NOT NULL DEFAULT '0',
  `n_entitle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`n_id`),
  KEY `tid` (`tid`,`n_hit`),
  KEY `v_addtime` (`n_inbase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_news` */

/*Table structure for table `nr_co_type` */

DROP TABLE IF EXISTS `nr_co_type`;

CREATE TABLE `nr_co_type` (
  `tid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tname` varchar(50) NOT NULL DEFAULT '',
  `siteurl` char(200) NOT NULL DEFAULT '',
  `getherday` smallint(5) unsigned NOT NULL DEFAULT '0',
  `playfrom` varchar(50) NOT NULL DEFAULT '',
  `autocls` enum('0','1') NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `coding` varchar(10) NOT NULL DEFAULT 'gb2312',
  `sock` enum('0','1') NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `cjtime` int(10) unsigned NOT NULL DEFAULT '0',
  `listconfig` text,
  `itemconfig` text,
  `isok` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`),
  KEY `cid` (`cid`,`classid`),
  KEY `addtime` (`addtime`,`cjtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_type` */

/*Table structure for table `nr_co_url` */

DROP TABLE IF EXISTS `nr_co_url`;

CREATE TABLE `nr_co_url` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `url` char(255) NOT NULL DEFAULT '',
  `pic` char(255) NOT NULL DEFAULT '',
  `succ` enum('0','1') NOT NULL DEFAULT '0',
  `err` int(5) NOT NULL DEFAULT '0',
  `cotype` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `cid` (`cid`,`tid`),
  KEY `succ` (`succ`,`err`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_co_url` */

/*Table structure for table `nr_comment` */

DROP TABLE IF EXISTS `nr_comment`;

CREATE TABLE `nr_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `typeid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `ip` char(15) NOT NULL DEFAULT '',
  `ischeck` smallint(6) NOT NULL DEFAULT '0',
  `dtime` int(10) unsigned NOT NULL DEFAULT '0',
  `msg` text,
  `m_type` int(6) unsigned NOT NULL DEFAULT '0',
  `reply` int(6) unsigned NOT NULL DEFAULT '0',
  `agree` int(6) unsigned NOT NULL DEFAULT '0',
  `anti` int(6) unsigned NOT NULL DEFAULT '0',
  `pic` char(255) NOT NULL DEFAULT '',
  `vote` int(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `v_id` (`v_id`,`ischeck`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_comment` */

/*Table structure for table `nr_content` */

DROP TABLE IF EXISTS `nr_content`;

CREATE TABLE `nr_content` (
  `v_id` mediumint(8) NOT NULL DEFAULT '0',
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `body` mediumtext,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_content` */

insert  into `nr_content`(`v_id`,`tid`,`body`) values (1,5,'aaaaaaaaaa');

/*Table structure for table `nr_count` */

DROP TABLE IF EXISTS `nr_count`;

CREATE TABLE `nr_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userip` varchar(16) DEFAULT NULL,
  `serverurl` varchar(255) DEFAULT NULL,
  `updatetime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `nr_count` */

/*Table structure for table `nr_crons` */

DROP TABLE IF EXISTS `nr_crons`;

CREATE TABLE `nr_crons` (
  `cronid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `available` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('user','system') NOT NULL DEFAULT 'user',
  `name` char(50) NOT NULL DEFAULT '',
  `filename` char(255) NOT NULL DEFAULT '',
  `lastrun` int(10) unsigned NOT NULL DEFAULT '0',
  `nextrun` int(10) unsigned NOT NULL DEFAULT '0',
  `weekday` tinyint(1) NOT NULL DEFAULT '0',
  `day` tinyint(2) NOT NULL DEFAULT '0',
  `hour` tinyint(2) NOT NULL DEFAULT '0',
  `minute` char(36) NOT NULL DEFAULT '',
  PRIMARY KEY (`cronid`),
  KEY `nextrun` (`available`,`nextrun`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_crons` */

/*Table structure for table `nr_data` */

DROP TABLE IF EXISTS `nr_data`;

CREATE TABLE `nr_data` (
  `v_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_name` char(60) NOT NULL DEFAULT '',
  `v_state` int(10) unsigned NOT NULL DEFAULT '0',
  `v_pic` char(255) NOT NULL DEFAULT '',
  `v_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_money` smallint(6) NOT NULL DEFAULT '0',
  `v_rank` smallint(6) NOT NULL DEFAULT '0',
  `v_digg` smallint(6) NOT NULL DEFAULT '0',
  `v_tread` smallint(6) NOT NULL DEFAULT '0',
  `v_commend` smallint(6) NOT NULL DEFAULT '0',
  `v_wrong` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_ismake` smallint(1) unsigned NOT NULL DEFAULT '0',
  `v_actor` varchar(200) DEFAULT NULL,
  `v_color` char(7) NOT NULL DEFAULT '',
  `v_publishyear` char(20) NOT NULL DEFAULT '0',
  `v_publisharea` char(20) NOT NULL DEFAULT '',
  `v_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `v_topic` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `v_note` char(30) NOT NULL DEFAULT '',
  `v_tags` char(30) NOT NULL DEFAULT '',
  `v_letter` char(3) NOT NULL DEFAULT '',
  `v_isunion` smallint(6) NOT NULL DEFAULT '0',
  `v_recycled` smallint(6) NOT NULL DEFAULT '0',
  `v_director` varchar(200) DEFAULT NULL,
  `v_enname` varchar(200) DEFAULT NULL,
  `v_lang` varchar(200) DEFAULT NULL,
  `v_score` bigint(10) DEFAULT '0',
  `v_scorenum` int(10) DEFAULT '0',
  `v_extratype` text,
  `v_jq` text,
  `v_nickname` char(60) DEFAULT NULL,
  `v_reweek` char(60) DEFAULT NULL,
  `v_douban` varchar(3) DEFAULT NULL,
  `v_mtime` varchar(3) DEFAULT NULL,
  `v_imdb` varchar(3) DEFAULT NULL,
  `v_tvs` char(60) DEFAULT NULL,
  `v_company` char(60) DEFAULT NULL,
  `v_dayhit` int(10) DEFAULT NULL,
  `v_weekhit` int(10) DEFAULT NULL,
  `v_monthhit` int(10) DEFAULT NULL,
  `v_daytime` int(10) DEFAULT NULL,
  `v_weektime` int(10) DEFAULT NULL,
  `v_monthtime` int(10) DEFAULT NULL,
  `v_len` varchar(6) DEFAULT NULL,
  `v_total` varchar(6) DEFAULT NULL,
  `v_ver` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`v_id`),
  KEY `idx_tid` (`tid`,`v_recycled`,`v_addtime`),
  KEY `idx_addtime` (`v_addtime`),
  KEY `idx_name` (`v_name`,`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nr_data` */

insert  into `nr_data`(`v_id`,`tid`,`v_name`,`v_state`,`v_pic`,`v_hit`,`v_money`,`v_rank`,`v_digg`,`v_tread`,`v_commend`,`v_wrong`,`v_ismake`,`v_actor`,`v_color`,`v_publishyear`,`v_publisharea`,`v_addtime`,`v_topic`,`v_note`,`v_tags`,`v_letter`,`v_isunion`,`v_recycled`,`v_director`,`v_enname`,`v_lang`,`v_score`,`v_scorenum`,`v_extratype`,`v_jq`,`v_nickname`,`v_reweek`,`v_douban`,`v_mtime`,`v_imdb`,`v_tvs`,`v_company`,`v_dayhit`,`v_weekhit`,`v_monthhit`,`v_daytime`,`v_weektime`,`v_monthtime`,`v_len`,`v_total`,`v_ver`) values (1,5,'测试硬盘',0,'',8,0,0,0,0,0,0,0,'a','','2015','大陆',1446353007,0,'ab','bb','C',0,0,'a','ceshi','国语',0,0,'1,5','','','','0','0','0','','',8,8,8,1446350277,1446350277,1446350277,'','20','高清版');

/*Table structure for table `nr_erradd` */

DROP TABLE IF EXISTS `nr_erradd`;

CREATE TABLE `nr_erradd` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vid` mediumint(8) unsigned NOT NULL,
  `author` char(60) NOT NULL DEFAULT '',
  `ip` char(15) NOT NULL DEFAULT '',
  `errtxt` mediumtext,
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_erradd` */

/*Table structure for table `nr_favorite` */

DROP TABLE IF EXISTS `nr_favorite`;

CREATE TABLE `nr_favorite` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `vid` int(11) NOT NULL DEFAULT '0',
  `kptime` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_favorite` */

/*Table structure for table `nr_flink` */

DROP TABLE IF EXISTS `nr_flink`;

CREATE TABLE `nr_flink` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `sortrank` smallint(6) NOT NULL DEFAULT '0',
  `url` char(60) NOT NULL DEFAULT '',
  `webname` char(30) NOT NULL DEFAULT '',
  `msg` char(200) NOT NULL DEFAULT '',
  `email` char(50) NOT NULL DEFAULT '',
  `logo` char(60) NOT NULL DEFAULT '',
  `dtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ischeck` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nr_flink` */

insert  into `nr_flink`(`id`,`sortrank`,`url`,`webname`,`msg`,`email`,`logo`,`dtime`,`ischeck`) values (1,0,'http://www.duomicms.net','多米影视管理系统','','','',1432312055,1);

/*Table structure for table `nr_guestbook` */

DROP TABLE IF EXISTS `nr_guestbook`;

CREATE TABLE `nr_guestbook` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title` varchar(60) NOT NULL DEFAULT '',
  `mid` mediumint(8) unsigned DEFAULT '0',
  `posttime` int(10) unsigned NOT NULL DEFAULT '0',
  `uname` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `dtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ischeck` smallint(6) NOT NULL DEFAULT '1',
  `msg` text,
  PRIMARY KEY (`id`),
  KEY `ischeck` (`ischeck`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_guestbook` */

/*Table structure for table `nr_jqtype` */

DROP TABLE IF EXISTS `nr_jqtype`;

CREATE TABLE `nr_jqtype` (
  `tid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `tname` char(30) NOT NULL DEFAULT '',
  `ishidden` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_jqtype` */

/*Table structure for table `nr_member` */

DROP TABLE IF EXISTS `nr_member`;

CREATE TABLE `nr_member` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `nickname` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` char(255) NOT NULL DEFAULT '',
  `logincount` smallint(6) NOT NULL DEFAULT '0',
  `regip` varchar(16) NOT NULL DEFAULT '',
  `regtime` int(10) NOT NULL DEFAULT '0',
  `gid` smallint(4) NOT NULL,
  `points` int(10) NOT NULL DEFAULT '0',
  `state` smallint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nr_member` */

insert  into `nr_member`(`id`,`username`,`nickname`,`password`,`email`,`logincount`,`regip`,`regtime`,`gid`,`points`,`state`) values (1,'zhangsan','','c3949ba59abbe56e057f','522157542@qq.com',3,'',1446351820,2,0,1);

/*Table structure for table `nr_member_group` */

DROP TABLE IF EXISTS `nr_member_group`;

CREATE TABLE `nr_member_group` (
  `gid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gname` varchar(32) NOT NULL DEFAULT '',
  `gtype` varchar(255) NOT NULL DEFAULT '',
  `g_auth` varchar(32) NOT NULL DEFAULT '',
  `g_upgrade` int(11) NOT NULL DEFAULT '0',
  `g_authvalue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `nr_member_group` */

insert  into `nr_member_group`(`gid`,`gname`,`gtype`,`g_auth`,`g_upgrade`,`g_authvalue`) values (1,'游客用户','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15','1,2,3',0,0),(2,'普通会员','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15','1,2,3',10,0);

/*Table structure for table `nr_myad` */

DROP TABLE IF EXISTS `nr_myad`;

CREATE TABLE `nr_myad` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `adname` varchar(100) NOT NULL DEFAULT '',
  `adenname` varchar(60) NOT NULL DEFAULT '',
  `timeset` int(10) unsigned NOT NULL DEFAULT '0',
  `intro` char(255) NOT NULL DEFAULT '',
  `adsbody` text,
  PRIMARY KEY (`aid`),
  KEY `timeset` (`timeset`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nr_myad` */

insert  into `nr_myad`(`aid`,`adname`,`adenname`,`timeset`,`intro`,`adsbody`) values (1,'article-right-ad','article-right-ad',1444999917,'内容页右侧广告位300*300','document.writeln(\"<img src=\"/duomiui/default/images/ad-300.jpg\">\")');

/*Table structure for table `nr_mytag` */

DROP TABLE IF EXISTS `nr_mytag`;

CREATE TABLE `nr_mytag` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tagname` varchar(30) NOT NULL DEFAULT '',
  `tagdes` varchar(50) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `tagcontent` text,
  PRIMARY KEY (`aid`),
  KEY `tagname` (`tagname`,`addtime`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `nr_mytag` */

insert  into `nr_mytag`(`aid`,`tagname`,`tagdes`,`addtime`,`tagcontent`) values (1,'areasearch','地区搜索',1251590919,'<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=大陆\' target=\"_blank\">大陆</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=香港\'target=\"_blank\">香港</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=台湾\'target=\"_blank\">台湾</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=日本\' target=\"_blank\">日本</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=韩国\' target=\"_blank\">韩国</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=欧美\' target=\"_blank\">欧美</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=2&searchword=其它\' target=\"_blank\">其它</a>'),(2,'yearsearch','按发行年份查看电影',1251509338,'<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2009\' target=\"_blank\">2009</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2008\'target=\"_blank\">2008</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2007\' target=\"_blank\">2007</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2006\' target=\"_blank\">2006</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2005\' target=\"_blank\">2005</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2004\' target=\"_blank\">2004</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2003\' target=\"_blank\">2003</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2002\' target=\"_blank\">2002</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=3&searchword=2001\' target=\"_blank\">2001</a>'),(3,'actorsearch','演员名字',1251590973,'<a href=\'/{duomicms:sitepath}search.php?searchtype=1&searchword=成龙\' target=\"_blank\">成龙</a> \r\n<a href=\'/{duomicms:sitepath}search.php?searchtype=1&searchword=周星驰\'target=\"_blank\">周星驰</a> ');

/*Table structure for table `nr_news` */

DROP TABLE IF EXISTS `nr_news`;

CREATE TABLE `nr_news` (
  `n_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `n_title` char(60) NOT NULL DEFAULT '',
  `n_pic` char(100) NOT NULL DEFAULT '',
  `n_hit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `n_money` smallint(6) NOT NULL DEFAULT '0',
  `n_rank` smallint(6) NOT NULL DEFAULT '0',
  `n_digg` smallint(6) NOT NULL DEFAULT '0',
  `n_tread` smallint(6) NOT NULL DEFAULT '0',
  `n_commend` smallint(6) NOT NULL DEFAULT '0',
  `n_author` varchar(80) DEFAULT NULL,
  `n_color` char(7) NOT NULL DEFAULT '',
  `n_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `n_note` smallint(6) NOT NULL DEFAULT '0',
  `n_letter` char(3) NOT NULL DEFAULT '',
  `n_isunion` smallint(6) NOT NULL DEFAULT '0',
  `n_recycled` smallint(6) NOT NULL DEFAULT '0',
  `n_entitle` varchar(200) DEFAULT NULL,
  `n_outline` varchar(255) DEFAULT NULL,
  `n_keyword` varchar(80) DEFAULT NULL,
  `n_from` varchar(50) DEFAULT NULL,
  `n_score` bigint(10) DEFAULT '0',
  `n_scorenum` int(10) DEFAULT '0',
  `n_content` mediumtext,
  PRIMARY KEY (`n_id`),
  KEY `tid` (`n_rank`,`tid`,`n_commend`,`n_hit`),
  KEY `v_addtime` (`n_addtime`,`n_digg`,`n_tread`,`n_isunion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_news` */

/*Table structure for table `nr_playdata` */

DROP TABLE IF EXISTS `nr_playdata`;

CREATE TABLE `nr_playdata` (
  `v_id` mediumint(8) NOT NULL DEFAULT '0',
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `body` mediumtext,
  `body1` mediumtext,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_playdata` */

insert  into `nr_playdata`(`v_id`,`tid`,`body`,`body1`) values (1,5,'ckplayer$$第1集$http://localhost/duomi/video/hwcheck.mp4$ckplayer#第2集$http://localhost/duomi/video/hwcheck.mp4$ckplayer#第3集$http://localhost/duomi/video/hwcheck.mp4$ckplayer','');

/*Table structure for table `nr_search_keywords` */

DROP TABLE IF EXISTS `nr_search_keywords`;

CREATE TABLE `nr_search_keywords` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` char(30) NOT NULL DEFAULT '',
  `spwords` char(50) NOT NULL DEFAULT '',
  `count` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `result` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_search_keywords` */

/*Table structure for table `nr_tags` */

DROP TABLE IF EXISTS `nr_tags`;

CREATE TABLE `nr_tags` (
  `tagid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag` char(30) NOT NULL DEFAULT '',
  `usenum` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `vids` text NOT NULL,
  PRIMARY KEY (`tagid`),
  KEY `usenum` (`usenum`),
  KEY `tag` (`tag`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `nr_tags` */

insert  into `nr_tags`(`tagid`,`tag`,`usenum`,`vids`) values (1,'bb',6,'1,1,1,1,1,1');

/*Table structure for table `nr_temp` */

DROP TABLE IF EXISTS `nr_temp`;

CREATE TABLE `nr_temp` (
  `v_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` smallint(8) unsigned NOT NULL DEFAULT '0',
  `v_name` char(60) NOT NULL DEFAULT '',
  `v_state` int(10) unsigned NOT NULL DEFAULT '0',
  `v_pic` char(100) NOT NULL DEFAULT '',
  `v_actor` varchar(200) DEFAULT NULL,
  `v_publishyear` char(20) NOT NULL DEFAULT '0',
  `v_publisharea` char(20) NOT NULL DEFAULT '',
  `v_addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `v_note` char(30) NOT NULL DEFAULT '',
  `v_letter` char(3) NOT NULL DEFAULT '',
  `v_playdata` mediumtext,
  `v_des` mediumtext,
  `v_director` varchar(200) DEFAULT NULL,
  `v_enname` varchar(200) DEFAULT NULL,
  `v_lang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`v_id`),
  KEY `tid` (`tid`),
  KEY `v_addtime` (`v_addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_temp` */

/*Table structure for table `nr_topic` */

DROP TABLE IF EXISTS `nr_topic`;

CREATE TABLE `nr_topic` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL DEFAULT '',
  `enname` char(60) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0',
  `template` char(50) NOT NULL DEFAULT '',
  `pic` char(80) NOT NULL DEFAULT '',
  `des` text,
  `vod` text NOT NULL,
  `keyword` text,
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `nr_topic` */

/*Table structure for table `nr_type` */

DROP TABLE IF EXISTS `nr_type`;

CREATE TABLE `nr_type` (
  `tid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `upid` tinyint(6) unsigned NOT NULL DEFAULT '0',
  `tname` char(30) NOT NULL DEFAULT '',
  `tenname` char(60) NOT NULL DEFAULT '',
  `torder` int(11) NOT NULL DEFAULT '0',
  `templist` char(50) NOT NULL DEFAULT '',
  `templist_1` char(50) NOT NULL DEFAULT '',
  `title` char(50) NOT NULL DEFAULT '',
  `keyword` char(50) NOT NULL DEFAULT '',
  `description` char(50) NOT NULL DEFAULT '',
  `ishidden` smallint(6) NOT NULL DEFAULT '0',
  `unionid` mediumtext,
  `tptype` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`),
  KEY `upid` (`upid`,`ishidden`),
  KEY `torder` (`torder`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `nr_type` */

insert  into `nr_type`(`tid`,`upid`,`tname`,`tenname`,`torder`,`templist`,`templist_1`,`title`,`keyword`,`description`,`ishidden`,`unionid`,`tptype`) values (1,0,'电影','video',1,'channel.html','content.html','','','',0,'31_1',0),(2,0,'电视剧','tv',2,'channel.html','content.html','','','',0,'',0),(3,0,'综艺','variety',3,'channel.html','content.html','','','',0,'',0),(4,0,'动漫','animation',4,'channel.html','content.html','','','',0,'',0),(5,1,'动作片','action',5,'channel.html','content.html','','','',0,'',0),(6,1,'喜剧片','comedy',6,'channel.html','content.html','','','',0,'',0),(7,1,'爱情片','love',7,'channel.html','content.html','','','',0,'',0),(8,1,'科幻片','fiction',8,'channel.html','content.html','','','',0,'',0),(9,1,'剧情片','plot',9,'channel.html','content.html','','','',0,'',0),(10,1,'恐怖片','terror',10,'channel.html','content.html','','','',0,'',0),(11,1,'战争片','warfare',11,'channel.html','content.html','','','',0,'',0),(12,2,'大陆剧','mainland',13,'channel.html','content.html','','','',0,'',0),(13,2,'港台剧','gangtai',14,'channel.html','content.html','','','',0,'',0),(14,2,'日韩剧','rihan',15,'channel.html','content.html','','','',0,'',0),(15,2,'欧美剧','oumei',16,'channel.html','content.html','','','',0,'',0),(16,0,'影视资讯','videonews',17,'newspage.html','news.html','','','',0,'',1),(17,0,'剧情介绍','videoplot',18,'newspage.html','news.html','','','',0,'',1);

/*新增表记录*/

CREATE TABLE `dm_member_seek` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '用户编号',
  `name` varchar(512) COLLATE utf8_general_mysql500_ci DEFAULT NULL COMMENT '片名',
  `act` varchar(512) COLLATE utf8_general_mysql500_ci DEFAULT NULL COMMENT '主演',
  `ctime` bigint(20) DEFAULT NULL COMMENT '发布时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '0未审核 1已处理 2不处理',
  `video` int(10) DEFAULT NULL COMMENT '片子编号',
  `video_url` varchar(512) COLLATE utf8_general_mysql500_ci DEFAULT NULL COMMENT '播放地址',
  `pass_msg` varchar(512) COLLATE utf8_general_mysql500_ci DEFAULT NULL COMMENT '不处理原因',
  `last_time` bigint(20) DEFAULT NULL COMMENT '最后处理时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
