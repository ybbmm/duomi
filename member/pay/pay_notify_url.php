<?php
/*此页面包含跳转支付*/
require_once("../../duomiphp/common.php");
require_once(duomi_INC.'/core.class.php');
include  'pay_config.php';

$dir_path = dirname(__FILE__);
$order_id = @$_POST['order_id'];
//$order_id = 1;
		
insert_log($order_id."==>POST==>".serialize($_POST));
insert_log($order_id."==>GET==>".serialize($_GET));	
$req = 'cmd=_notify-validate';
foreach($_POST as $key=>$v){
	$req .= "&{$key}={$v}";
}
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $pay_url);
curl_setopt($ch, CURLOPT_HTTPHEADER, 'application/x-www-form-urlencoded');
curl_setopt($ch, CURLOPT_HTTPHEADER, 'HOST: '.$pay_url_host);
curl_setopt($ch, CURLOPT_HTTPHEADER, 'Content-Length: '.strlen($req));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
$response = curl_exec($ch);
if(curl_errno($ch)){
	echo "faile";
	curl_close($ch);
	
}
curl_close($ch);
if(strcmp ($response, "VERIFIED") == 0 ){
	insert_log($order_id.'==>validate==>SUCC');
	$pay_time = time();
	$payment_status = $_POST['payment_status'];
	$txn_id = $_POST['txn_id']; //交易号
	$mc_gross = $_POST['mc_gross']; //交易号
	$row1=$dsql->GetOne("select * from {$cfg_dbprefix}pay_record where id={$order_id}");
	if(!$row1){
		insert_log($order_id.'==>validate==>充值记录不存在==>付款金额'.$mc_gross);
		exit('faile');
		
	}
	if($row1['money'] != $mc_gross){
		insert_log($order_id.'==>validate==>已经付款但是付款有误==>付款金额'.$mc_gross);
		exit('faile');
	}
	if($row1['status'] == 1){
		insert_log($order_id.'==>validate==>已经付');
		exit('faile');
	}
	$row_group =$dsql->GetOne("select * from `{$cfg_dbprefix}member_group` where gid = {$row1['gid']}");
	if($row_group['g_upgrade'] <= 0){
		insert_log(order_id.'==>validate==>该用户组不允许充值');
		exit('faile');
		
	}
	$row_user =$dsql->GetOne("select * from `{$cfg_dbprefix}member` where id={$row1['uid']}");
	if(!$row_user){
		insert_log(order_id.'==>validate==>没有改充值用户');
		exit('faile');
		
	}
	$end_time = strtotime("+{$row_group['g_day']} day",$pay_time);
	if($row_user['end_time'] > $pay_time){
		$end_time = strtotime("+{$row_group['g_day']} day",$row_user['end_time']);
		
	}
	//更改付款记录
	$dsql->ExecuteNoneQuery("update `{$cfg_dbprefix}pay_record` set status = 1, paytime = {$pay_time}, tax_id = '{$txn_id}' where id= {$order_id}");
	//更改用户信息
	$dsql->ExecuteNoneQuery("update `{$cfg_dbprefix}member` set money = money+{$row1['money']}, end_time = {$end_time},gid ={$row1['gid']}  where id= '{$row1['uid']}'");
	
	
	echo "SUCC";
}else{
	insert_log($order_id.'==>validate==>FAIL');
	echo "FAIL";
}


function insert_log($content){
	$file_path = duomi_ROOT.'/data/pay.log';
	chmod($file_path, 0600); 
	$fp = fopen($file_path, 'a+');
	if($fp){
		fwrite($fp, date('Y-m-d H:i:s').' '.$content."\n");
		fclose($fp);
	}else{
		exit('sb FP');
	}
	
}
