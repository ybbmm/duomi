<?php
/*此页面包含跳转支付*/
require_once("../../duomiphp/common.php");

require_once(duomi_INC.'/core.class.php');

include  'pay_config.php';
$dir_path = dirname(__FILE__);
$order_type = @$_GET['order_type'];
$order_type = isset($order_type) ? trim($order_type) : 2;
session_start();

$uid = $_SESSION['duomi_user_id'];
$username = $_SESSION['duomi_user_name'];
$user_group = $_SESSION['duomi_user_group'];
if(empty($_SESSION['duomi_user_id']))
{
	showMsg("请先登录",$www_root_url."/member/login.php");
	exit();
}
//充值列表 需要设定好
$row1=$dsql->GetOne("select * from `{$cfg_dbprefix}member_group` where gid = {$order_type}");
if($row1['g_upgrade'] <= 0){
	showMsg("该用户组不允许充值",$www_root_url."/member/");
	exit();
	
}
$now_time = time();
$pay_money_record = $row1['gid'];
$sql = "INSERT INTO `{$cfg_dbprefix}pay_record` ( `uid`, `username`,`gid`, `money`, `ctime`, `paytime` ) VALUES ({$uid},'{$username}',{$order_type},{$pay_money_record},{$now_time},0 )";

//添加充值记录
$dsql->ExecuteNoneQuery($sql);
$pay_record_id = $dsql->GetLastID();
$templatePath = $dir_path."/pay_tpl/pay.html";
$pay_name = "购买{$row1['gname']}";
include $templatePath;